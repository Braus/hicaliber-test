require('babel-polyfill');
var path = require('path'),
    UglifyJsPlugin = require('uglifyjs-webpack-plugin'),
    ExtractTextPlugin = require('extract-text-webpack-plugin'),
    LiveReloadPlugin = require('webpack-livereload-plugin'),
    webpack = require('webpack');

module.exports = {
    entry: ['babel-polyfill', './js/main.js', './sass/style.scss'],
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'js/main.min.js',
        chunkFilename: 'js/main.min.js'
    },

    module: {
        rules: [
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: ['style-loader'],
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                importLoaders: 1,
                                minimize: true,
                                sourceMap: true,
                                outputPath: 'css/'
                            }
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                ident: 'postcss',
                                sourceMap: true,
                                plugins: [
                                    require('precss'),
                                    require('autoprefixer')
                                ]
                            },
                        },
                        {
                            loader: 'sass-loader',
                            options: { sourceMap: true }
                        }
                    ]
                })
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                }
            },
            {
                test: /\.(ttf|eot|woff|woff2|svg)$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: 'icons/',
                        publicPath: '../icons'
                    }
                }]
            }
        ]
    },
    resolve: {
        extensions: ['.js'],
    },
    plugins: [
        new ExtractTextPlugin({
            filename: 'css/main.min.css',
            allChunks: true
        }),
        new UglifyJsPlugin({
            uglifyOptions: {
                ie8: true,
                safari10: true,
                mangle: false,
                output: {
                    comments: false
                }
            },
            sourceMap: true
        }),
        new LiveReloadPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.SourceMapDevToolPlugin({})
    ]
}