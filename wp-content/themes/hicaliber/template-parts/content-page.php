<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hicaliber
 */


$title = get_field( 'title' );
$subtitle = get_field( 'subtitle' );
$trend_posts_id = get_field( 'add_id_to_section' );
$trend_classes = get_field( 'additional_section_classes' );
$trend_category = get_field( 'posts_category' );
$image_position = get_field( 'image_position' );
$post_per_row = get_field( 'posts_per_row' );

if ( $post_per_row === "1") :
	$col = 12;
elseif ( $post_per_row === "2") :
	$col = 6;
elseif ( $post_per_row === "3") :
	$col = 4;
else :
	$col = 3;
endif;

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<?php hicaliber_post_thumbnail(); ?>

	<div class="entry-content">

		<?php the_content(); ?>

		<?php if ( $title || $subtitle ) : ?>
			<section <?php echo $trend_posts_id ? 'id=' . $trend_posts_id : null; ?> class="pt-4 pb-4 <?php echo $trend_classes; ?>">
				<?php if( $title ) : ?>
					<h2 class="text-center m-2"><?php echo $title; ?></h2>
				<?php endif; ?>

				<?php if( $subtitle ) : ?>
					<h3 class="text-center m-2"><?php echo $subtitle; ?></h3>
				<?php endif; ?>

				<div class="trend_posts mt-4">
					<div class="row">
						<?php $query = new WP_Query( array( 'cat' => $trend_category ) ); ?>

						<?php if ( $query->have_posts() ) : ?>

							<?php while( $query->have_posts() ) : $query->the_post(); ?>

								<div class="col-<?php echo $col;?> mb-4">
									<div class="<?php echo $image_position; ?>">
										<?php if ( has_post_thumbnail( $post->ID ) )  : ?>
											<div class="image" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>)"></div>
										<?php endif; ?>
										<div class="<?php echo $image_position === 'top' ? 'text-center' : null; ?> mt-3 info">
											<h4><strong class="mb-3"><?php the_title(); ?></strong></h4>
											<?php the_excerpt(); ?>
										</div>
									</div>
								</div>



							<?php endwhile; ?>
						<?php endif; ?>

						<?php  wp_reset_postdata(); ?>
					</div>
				</div>
			</section>
		<?php endif; ?>


		<?php wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'hicaliber' ),
			'after'  => '</div>',
		) );
		?>
	</div><!-- .entry-content -->

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
			edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Edit <span class="screen-reader-text">%s</span>', 'hicaliber' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->
