<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

if ( file_exists( dirname( __FILE__ ) . '/local-config.php' ) ) {

	define( 'WP_LOCAL_DEV', true );
	define( 'WP_STAGING_DEV', false );
	include( dirname( __FILE__ ) . '/local-config.php' );

} else {
	// ** MySQL settings - You can get this info from your web host ** //
	/** The name of the database for WordPress */
	define( 'DB_NAME', 'nomaddi4_hicaliber' );

	/** MySQL database username */
	define( 'DB_USER', 'nomaddi4_caliber' );

	/** MySQL database password */
	define( 'DB_PASSWORD', 'MX9rMPcwfTDQLz9' );

	/** MySQL hostname */
	define( 'DB_HOST', 'localhost' );

	/** Database Charset to use in creating database tables. */
	define( 'DB_CHARSET', 'utf8mb4' );

	/** The Database Collate type. Don't change this if in doubt. */
	define( 'DB_COLLATE', '' );
}



/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'c}<16-;r{,W_g[&SV[{5g3795N4xF~W(<32M*lMP^7^I3<1Kz6ME9A;-lU~yu8T(' );
define( 'SECURE_AUTH_KEY',  ':<g/9jWsRf_GyW`xt:7cmy &68ZfDAf*}|*(b{iuCOMN2 6%AV3@D5&?H<CSp<sa' );
define( 'LOGGED_IN_KEY',    '_xB=>}YyiI*7>T#?l%oLY^udpkg[vA{4CjyJ`Qj9q&z|~[V)XmpAV~&o^Kw5Juj ' );
define( 'NONCE_KEY',        'x!iPpS=F68-%-P^+r4c=q`d/{j6QtVG/YUBYZa`;_8sh>W3U(fA~ja9PdGn6#UzC' );
define( 'AUTH_SALT',        '>`gl)Y}dV+<$Ycvqr:n5#+_SC&!Bf=;q/f`Qtx>.<Om|NRYF!W7R%K<AY1:%WwQJ' );
define( 'SECURE_AUTH_SALT', 'I.J^A3<@:L=K`8:gx=VT2R#^:1IfP}0W8}{Tb{SDR|>n?Q-LtJ9y-d_*O;p%aO*:' );
define( 'LOGGED_IN_SALT',   '6 vsw=nZ-ww*7V-)?r0r.aE8ff$dKS2d#3oDk7WzuarpyyxVpa=4Y~HV~P}37Vbr' );
define( 'NONCE_SALT',       '3ftKj _y3-rAL*OuDZt%uOBPq<sx><#0iB*SRz5bS6cRC]%LzP7=DM]*Pj)N&tHJ' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
