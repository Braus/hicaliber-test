<?php
define( 'DB_NAME', 'DATABASE_NAME' );
define( 'DB_USER', 'USERNAME' );
define( 'DB_PASSWORD', 'PASSWORD' );
define( 'DB_HOST', 'LOCALHOST' );

define( 'WP_DEBUG', true );
define( 'WP_DEBUG_DISPLAY', true );

define( 'WP_SITEURL', 'http://example.local/' );
define( 'WP_HOME', 'http://example.local/' );
